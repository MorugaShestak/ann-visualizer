class Car {
 constructor(x,y,width,height, controlType, maxSpeed=3){
     this.x=x; //координата x
     this.y=y; //координата y
     this.width=width; //ширина
     this.height=height; //высота

     this.speed=0; //скорость
     this.acceleration=0.2; //ускорение
     this.maxSpeed=maxSpeed; //максимальная скорость
     this.friction=0.05; //трение
     this.angle=0; //угол поворота
     this.damaged = false; //поврежден ли автомобиль

     this.useBrain = controlType == "AI"; //используется ли искусственный интеллект

     if(controlType !== "DUMMY") { //если тип управления не DUMMY
         this.sensor = new Sensor(this); //создаем датчик
         this.brain = new NeuralNetwork([this.sensor.rayCount, 6, 4]); //создаем нейронную сеть
     }

     this.controls = new Controls(controlType); //создаем элементы управления
 }

 update(roadBorders, traffic){
     if(!this.damaged) { //если автомобиль не поврежден
         this.#move(); //двигаем автомобиль
         this.polygon = this.#createPolygon(); //создаем многоугольник
         this.damaged = this.#assessDamage(roadBorders, traffic); //оцениваем повреждения
     }
     if(this.sensor) { //если есть датчик
         this.sensor.update(roadBorders, traffic); //обновляем датчик
         const offsets = this.sensor.readings.map(s => s == null ? 0 : 1 - s.offset); //считываем смещения
         const outputs = NeuralNetwork.feedForward(offsets, this.brain); //получаем выходные данные нейронной сети

         if (this.useBrain) { //если используется искусственный интеллект
             this.controls.forward = outputs[0]; //передвижение вперед
             this.controls.left = outputs[1]; //поворот налево
             this.controls.right = outputs[2]; //поворот направо
             this.controls.reverse = outputs[3]; //движение назад
         }
     }
 }

 #createPolygon(){ //создание многоугольника
     const points=[];
     const rad=Math.hypot(this.width,this.height)/2;
     const alpha=Math.atan2(this.width,this.height);
     points.push({
         x:this.x-Math.sin(this.angle-alpha)*rad,
         y:this.y-Math.cos(this.angle-alpha)*rad
     });
     points.push({
         x:this.x-Math.sin(this.angle+alpha)*rad,
         y:this.y-Math.cos(this.angle+alpha)*rad
     });
     points.push({
         x:this.x-Math.sin(Math.PI+this.angle-alpha)*rad,
         y:this.y-Math.cos(Math.PI+this.angle-alpha)*rad
     });
     points.push({
         x:this.x-Math.sin(Math.PI+this.angle+alpha)*rad,
         y:this.y-Math.cos(Math.PI+this.angle+alpha)*rad
     });
     return points;
 }
     #assessDamage(roadBorders, traffic) { //оценка повреждений
     for(let i = 0; i < roadBorders.length; i++) {
         if(polyIntersect(this.polygon, roadBorders[i])) { //если произошло пересечение с границей дороги
             return true; //возвращаем true
         }
     }

         for(let i = 0; i < traffic.length; i++) {
             if(polyIntersect(this.polygon, traffic[i].polygon)) { //если произошло пересечение с другим автомобилем
                 return true; //возвращаем true
             }
         }
     return false; //возвращаем false
 }

 #move(){ //движение автомобиля
     if(this.controls.forward){ //если движение вперед
         this.speed+=this.acceleration; //увеличиваем скорость
     }
     if(this.controls.reverse){ //если движение назад
         this.speed-=this.acceleration; //уменьшаем скорость
     }

     if(this.speed>this.maxSpeed){ //если скорость больше максимальной
         this.speed=this.maxSpeed; //скорость равна максимальной
     }
     if(this.speed<-this.maxSpeed/2){ //если скорость меньше половины максимальной
         this.speed=-this.maxSpeed/2; //скорость равна половине максимальной
     }

     if(this.speed>0){ //если скорость больше 0
         this.speed-=this.friction; //уменьшаем скорость
     }
     if(this.speed<0){ //если скорость меньше 0
         this.speed+=this.friction; //увеличиваем скорость
     }
     if(Math.abs(this.speed)<this.friction){ //если скорость меньше трения
         this.speed=0; //скорость равна 0
     }

     if(this.speed!==0){ //если скорость не равна 0
         const flip=this.speed>0?1:-1; //если скорость больше 0, flip равен 1, иначе -1
         if(this.controls.left){ //если поворот налево
             this.angle+=0.03*flip; //изменяем угол поворота
         }
         if(this.controls.right){ //если поворот направо
             this.angle-=0.03*flip; //изменяем угол поворота
         }
     }

     this.x-=Math.sin(this.angle)*this.speed; //изменяем координату x
     this.y-=Math.cos(this.angle)*this.speed; //изменяем координату y
 }

    draw(ctx, color, drawSensor=false){ // Отрисовка автомобиля
        if(this.damaged) {
            ctx.fillStyle = "gray";
        } else {
            ctx.fillStyle = color;
        }
        ctx.beginPath();
        ctx.moveTo(this.polygon[0].x, this.polygon[0].y);
        for(let i = 1; i < this.polygon.length; i++) {
            ctx.lineTo(this.polygon[i].x, this.polygon[i].y)
        }
        ctx.fill();
        if(this.sensor && drawSensor) {
            this.sensor.draw(ctx);
        }
    }
}
