// Класс для визуализации нейронных сетей
class Visualizer{
 // Метод для отрисовки сети
 drawNetwork(ctx,network){
     // Задаем отступы и размеры области для рисования
     const margin=50;
     const left=margin;
     const top=margin;
     const width=ctx.canvas.width-margin*2;
     const height=ctx.canvas.height-margin*2;

     // Вычисляем высоту уровня
     const levelHeight=height/network.levels.length;

     // Проходим по всем уровням сети
     for(let i=network.levels.length-1;i>=0;i--){
         // Вычисляем верхнюю координату уровня
         const levelTop=top+
             lerp(
                 height-levelHeight,
                 0,
                 network.levels.length==1
                     ?0.5
                     :i/(network.levels.length-1)
             );

         // Задаем штриховую линию и рисуем уровень
         ctx.setLineDash([7,3]);
         Visualizer.drawLevel(ctx,network.levels[i],
             left,levelTop,
             width,levelHeight,
             i==network.levels.length-1
                 ?['🠉','🠈','🠊','🠋']
                 :[]
         );
     }
 }

 // Метод для отрисовки уровня
 static drawLevel(ctx,level,left,top,width,height,outputLabels){
     // Вычисляем координаты углов прямоугольника уровня
     const right=left+width;
     const bottom=top+height;

     // Получаем входы, выходы, веса и смещения уровня
     const {inputs,outputs,weights,biases}=level;

     // Проходим по всем входам и выходам и рисуем связи между ними
     for(let i=0;i<inputs.length;i++){
         for(let j=0;j<outputs.length;j++){
             ctx.beginPath();
             ctx.moveTo(
                 Visualizer.#getNodeX(inputs,i,left,right),
                 bottom
             );
             ctx.lineTo(
                 Visualizer.#getNodeX(outputs,j,left,right),
                 top
             );
             ctx.lineWidth=2;
             ctx.strokeStyle=getRGBA(weights[i][j]);
             ctx.stroke();
         }
     }

     // Рисуем узлы входов
     const nodeRadius=18;
     for(let i=0;i<inputs.length;i++){
         const x=Visualizer.#getNodeX(inputs,i,left,right);
         ctx.beginPath();
         ctx.arc(x,bottom,nodeRadius,0,Math.PI*2);
         ctx.fillStyle="black";
         ctx.fill();
         ctx.beginPath();
         ctx.arc(x,bottom,nodeRadius*0.6,0,Math.PI*2);
         ctx.fillStyle=getRGBA(inputs[i]);
         ctx.fill();
     }

     // Рисуем узлы выходов
     for(let i=0;i<outputs.length;i++){
         const x=Visualizer.#getNodeX(outputs,i,left,right);
         ctx.beginPath();
         ctx.arc(x,top,nodeRadius,0,Math.PI*2);
         ctx.fillStyle="black";
         ctx.fill();
         ctx.beginPath();
         ctx.arc(x,top,nodeRadius*0.6,0,Math.PI*2);
         ctx.fillStyle=getRGBA(outputs[i]);
         ctx.fill();

         // Рисуем смещение выхода
         ctx.beginPath();
         ctx.lineWidth=2;
         ctx.arc(x,top,nodeRadius*0.8,0,Math.PI*2);
         ctx.strokeStyle=getRGBA(biases[i]);
         ctx.setLineDash([3,3]);
         ctx.stroke();
         ctx.setLineDash([]);

         // Рисуем метки выходов
         if(outputLabels[i]){
             ctx.beginPath();
             ctx.textAlign="center";
             ctx.textBaseline="middle";
             ctx.fillStyle="black";
             ctx.strokeStyle="white";
             ctx.font=(nodeRadius*1.5)+"px Arial";
             ctx.fillText(outputLabels[i],x,top+nodeRadius*0.1);
             ctx.lineWidth=0.5;
             ctx.strokeText(outputLabels[i],x,top+nodeRadius*0.1);
         }
     }
 }

 // Метод для вычисления координаты узла
 static #getNodeX(nodes,index,left,right){
     return lerp(
         left,
         right,
         nodes.length==1
             ?0.5
             :index/(nodes.length-1)
     );
 }
}
