// Класс Road описывает дорогу
class Road{
 // Конструктор принимает координату x, ширину дороги и количество полос
 constructor(x,width,laneCount=3){
     // Сохраняем переданные параметры
     this.x=x;
     this.width=width;
     this.laneCount=laneCount;

     // Вычисляем координаты границ дороги
     this.left=x-width/2;
     this.right=x+width/2;

     // Задаем координаты верхней и нижней границы дороги
     const infinity=1000000;
     this.top=-infinity;
     this.bottom=infinity;

     // Задаем координаты углов дороги
     const topLeft={x:this.left,y:this.top};
     const topRight={x:this.right,y:this.top};
     const bottomLeft={x:this.left,y:this.bottom};
     const bottomRight={x:this.right,y:this.bottom};
     this.borders=[
         [topLeft,bottomLeft],
         [topRight,bottomRight]
     ];
 }

 // Метод возвращает координату центра полосы по ее индексу
 getLaneCenter(laneIndex){
     const laneWidth=this.width/this.laneCount;
     return this.left+laneWidth/2+
         Math.min(laneIndex,this.laneCount-1)*laneWidth;
 }

 // Метод отрисовывает дорогу
 draw(ctx){
     // Задаем параметры линии
     ctx.lineWidth=5;
     ctx.strokeStyle="white";

     // Отрисовываем разметку на дороге
     for(let i=1;i<=this.laneCount-1;i++){
         const x=lerp(
             this.left,
             this.right,
             i/this.laneCount
         );

         ctx.setLineDash([20,20]);
         ctx.beginPath();
         ctx.moveTo(x,this.top);
         ctx.lineTo(x,this.bottom);
         ctx.stroke();
     }

     // Отрисовываем границы дороги
     ctx.setLineDash([]);
     this.borders.forEach(border=>{
         ctx.beginPath();
         ctx.moveTo(border[0].x,border[0].y);
         ctx.lineTo(border[1].x,border[1].y);
         ctx.stroke();
     });
 }
}
