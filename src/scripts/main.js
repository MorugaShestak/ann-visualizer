// Получаем элементы canvas из HTML-кода
const carCanvas = document.getElementById("carCanvas");
carCanvas.width = 200;

const networkCanvas = document.getElementById( "networkCanvas" );
networkCanvas.width = 300;

// Создаем объект Visualizer
const Visualizerr = new Visualizer();

// Получаем контексты для рисования на canvas
const carCtx = carCanvas.getContext("2d");
const networkCtx = networkCanvas.getContext("2d");

// Создаем объекты Road и Car
const road = new Road(carCanvas.width/2,carCanvas.width*0.9);
const car = new Car(road.getLaneCenter(1), 100, 30, 50, "AI", maxSpeed = 3);

// Создаем массив traffic, содержащий объекты Car
const traffic=[
 new Car(road.getLaneCenter(1),-100,30,50,"DUMMY",2),
 new Car(road.getLaneCenter(0),-300,30,50,"DUMMY",2),
 new Car(road.getLaneCenter(2),-300,30,50,"DUMMY",2),
 new Car(road.getLaneCenter(0),-500,30,50,"DUMMY",2),
 new Car(road.getLaneCenter(1),-500,30,50,"DUMMY",2),
 new Car(road.getLaneCenter(1),-700,30,50,"DUMMY",2),
 new Car(road.getLaneCenter(2),-700,30,50,"DUMMY",2),
];

// Генерируем массив cars, содержащий объекты Car
const N = 1000;
const cars = generateCars(N);
let bestCar = cars[0];

// Если в localStorage есть сохраненный лучший мозг, то загружаем его и мутируем остальные мозги
if (localStorage.getItem("bestBrain")) {
 for (let i = 0; i < cars.length; i++) {
     cars[i].brain = JSON.parse(localStorage.getItem("bestBrain"))
     if (i != 0) {
         NeuralNetwork.mutate(cars[i].brain, 0.3);
     }
 }
}

// Запускаем анимацию
animate();

// Функция для сохранения лучшего мозга в localStorage
function save() {
 localStorage.setItem("bestBrain", JSON.stringify(bestCar.brain));
 console.log("best Brain saved");
}

// Функция для удаления лучшего мозга из localStorage
function discard() {
 localStorage.removeItem("bestBrain")
 console.log("best Brain deleted")
}

// Функция перезапуска забега
function rerun() {
    refresh();
}

// Функция для генерации массива cars
function generateCars(N) {
 const cars = [];
 for (let i = 0; i < N; i++) {
     cars.push(new Car(road.getLaneCenter(1), 100, 30, 50, "AI"))
 }
 return cars
}

// Функция для перезагрузки страницы
function refresh() {
    for (const element of cars) {
        if (element.damaged) {
            element.damaged = false;
        }
    }
 document.location.reload(true)
}

// Функция для проверки поврежденных машин
function damagedCarsCheck(cars) {
 let damagedCarsCount = 0
 for (const element of cars) {
     if (element.damaged) {
         damagedCarsCount++;
     }
 }
 if (damagedCarsCount >= cars.length - 10) {
     console.log("all cars are dead")
     damagedCarsCount = 0;
     for (const element of cars) {
         if (element.damaged) {
             element.damaged = false;
         }
        }
     refresh();
 }
}

// Функция для анимации
function animate(time) {
 damagedCarsCheck(cars)

 // Обновляем положение машин в массиве traffic
 for(let i = 0; i < traffic.length; i++) {
     traffic[i].update(road.borders, []);
 }

 // Обновляем положение машин в массиве cars
 for (let i = 0; i < cars.length; i++) {
     cars[i].update(road.borders, traffic);
 }

 // Находим лучшую машину
    tempCar = cars.find(c => c.y === Math.min(...cars.map(c => c.y)));
    if (bestCar != tempCar) {
        bestCar = tempCar
        save()
    }

 // Обновляем положение машины car
 car.update(road.borders, traffic);

 // Устанавливаем высоту canvas равной высоте окна браузера
 carCanvas.height = window.innerHeight;
 networkCanvas.height = window.innerHeight;

 // Сохраняем текущее состояние контекста
 carCtx.save();
 // Смещаем контекст вверх, чтобы лучшая машина была видна на экране
 carCtx.translate(0,-bestCar.y+carCanvas.height*0.7);

 // Рисуем дорогу и машины из массива traffic
 road.draw(carCtx);
 for(let i = 0; i < traffic.length; i++) {
     traffic[i].draw(carCtx, "black");
 }

 // Устанавливаем прозрачность для машин из массива cars
 carCtx.globalAlpha = 0.2;

 // Рисуем машины из массива cars
 for (let i = 0; i < cars.length; i++) {
     cars[i].draw(carCtx, "blue");
 }

 // Устанавливаем прозрачность обратно в 1 и рисуем лучшую машину
 carCtx.globalAlpha = 1;
 bestCar.draw(carCtx, "blue", true);

 // Восстанавливаем сохраненное состояние контекста
 carCtx.restore();

 // Устанавливаем смещение для линий на canvas networkCanvas
 networkCtx.lineDashOffset = time/50;
 // Рисуем нейронную сеть лучшей машины
 Visualizerr.drawNetwork(networkCtx, bestCar.brain)

 // Запускаем анимацию заново
 requestAnimationFrame(animate);
}
